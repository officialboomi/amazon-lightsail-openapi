# Amazon Lightsail Connector
Amazon Lightsail is the easiest way to get started with Amazon Web Services (AWS) for developers who need to build websites or web applications.

Documentation: https://docs.aws.amazon.com/lightsail

Specification: https://github.com/APIs-guru/openapi-directory/blob/main/APIs/amazonaws.com/lightsail/2016-11-28/openapi.yaml

## Prerequisites

+ Generate an AWS Acess Key ID and AWS Secret Key from the AWS Admin Console

## Supported Operations
All endpoints are operational.
## Connector Feedback

Feedback can be provided directly to Product Management in our [Product Feedback Forum](https://community.boomi.com/s/ideas) in the boomiverse.  When submitting an idea, please provide the full connector name in the title and a detailed description.

